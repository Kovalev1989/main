
let time = 30 * 60;
const timerDiv = document.querySelector('.timer');
const timer = setInterval(function () {
    const seconds = time % 60;
    const minutes = time / 60 % 60;
    if (time <= 0) {
        clearInterval(timer);
    } else {
        const strTimer = `${Math.trunc(minutes)}:${seconds}`;
        timerDiv.innerHTML = strTimer;
    }
    --time;
}, 1000);